Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header is :
Date=Thu, 23 May 2024 18:17:33 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1716488253&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=TBgfcIZtmeWLYA4ha%2BaLo2j%2BYJQDCFikldNl6CKOP%2BE%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1716488253&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=TBgfcIZtmeWLYA4ha%2BaLo2j%2BYJQDCFikldNl6CKOP%2BE%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-NyOuzDroibhmqJtPY2noIso/cGM"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=88870b1c799429f3-BOM
Content-Encoding=gzip

Response body is :
{"name":"morpheus","job":"zion resident","updatedAt":"2024-05-23T18:17:33.075Z"}