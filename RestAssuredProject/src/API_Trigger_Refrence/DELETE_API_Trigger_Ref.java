package API_Trigger_Refrence;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class DELETE_API_Trigger_Ref {

	public static void main(String[] args) {
		// step 1 : Declare the needed variables
		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";

		//step 2 : Configure the API and trigger
		
		RestAssured.baseURI = hostname;
		
		 String responseBody = given().when().delete(resource).then().extract().response().asString();
		
		System.out.println(responseBody);	

	}

}
