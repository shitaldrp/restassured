package Run;
import java.io.IOException;

import TestScript.Delete_Test_Script;
import TestScript.Get_Test_Script;
import TestScript.Patch_Test_Script;
import TestScript.Post_Test_Script;
import TestScript.Post_param_Test_Script;
import TestScript.Put_Test_Script;
public class PostRun {

	public static void main(String[] args)throws IOException {
		Post_Test_Script.execute();
				Put_Test_Script.execute();
				Patch_Test_Script.execute();
				Get_Test_Script.execute();
				Delete_Test_Script.execute();
				//Post_param_Test_Script.execute();

	}

}
